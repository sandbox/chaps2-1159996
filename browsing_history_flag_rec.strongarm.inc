<?php

/**
 * Implementation of hook_strongarm().
 */
function browsing_history_flag_rec_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'flag_rec_browsing_history_content_item2item';
  $strongarm->value = array(
    'id' => 'browsing_history_content_item2item',
    'label' => 'Browsing history content itemt2item',
    'flag' => 'browsing_history_content',
    'algorithm' => 'item2item',
    'boost_recency' => 1,
    'days' => '23',
    'usertype' => 'authenticated',
  );

  $export['flag_rec_browsing_history_content_item2item'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'flag_rec_browsing_history_content_item2item_anon';
  $strongarm->value = array(
    'id' => 'browsing_history_content_item2item_anon',
    'label' => 'Browsing history content anonymous item 2 item',
    'flag' => 'browsing_history_content',
    'algorithm' => 'item2item',
    'boost_recency' => 1,
    'days' => '23',
    'usertype' => 'anonymous',
  );

  $export['flag_rec_browsing_history_content_item2item_anon'] = $strongarm;
  return $export;
}
