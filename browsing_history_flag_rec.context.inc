<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function browsing_history_flag_rec_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'browsing_history_content_recommender';
  $context->description = '';
  $context->tag = 'browsing_history_recommender';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'browsing_history_content' => 'browsing_history_content',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-content_similarity-block_1' => array(
          'module' => 'views',
          'delta' => 'content_similarity-block_1',
          'region' => 'content',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('browsing_history_recommender');

  $export['browsing_history_content_recommender'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'browsing_history_recommender_anon_user_page';
  $context->description = '';
  $context->tag = 'browsing_history_recommender';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'browsing_history_user_page' => 'browsing_history_user_page',
      ),
    ),
    'user' => array(
      'values' => array(
        'anonymous user' => 'anonymous user',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-content_predictions2-block_2' => array(
          'module' => 'views',
          'delta' => 'content_predictions2-block_2',
          'region' => 'content',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('browsing_history_recommender');

  $export['browsing_history_recommender_anon_user_page'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'browsing_history_recommender_user_page';
  $context->description = '';
  $context->tag = 'browsing_history_recommender';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'browsing_history_user_page' => 'browsing_history_user_page',
      ),
    ),
    'user' => array(
      'values' => array(
        'authenticated user' => 'authenticated user',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-content_predictions2-block_1' => array(
          'module' => 'views',
          'delta' => 'content_predictions2-block_1',
          'region' => 'content',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('browsing_history_recommender');

  $export['browsing_history_recommender_user_page'] = $context;
  return $export;
}
